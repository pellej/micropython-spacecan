config = {
  "DEBUG": True,

  # Redundancy management
  "TIMER_HEARTBEAT": 1,  # timer of pyboard
  "LED_HEARTBEAT": 1,  # set to None for off
  "HEARTBEAT_PERIOD": 500,
  "MAX_HEARTBEAT_MISSED": 3,  # switch bus after x frames missed
  "MAX_BUS_SWITCHES": False,  # set to False for unlimited

  # Sync service
  "TIMER_SYNC": 4,  # timer of pyboard
  "LED_SYNC": 2,  # set to None for off

  # TC/TM service
  "TC_BUFFER_SIZE": 10,  # tc frames to buffer (at slave node)
  "TM_BUFFER_SIZE": 50,  # tm frames to buffer (at master node)
  "FRAME_BUFFER_SIZE": 10,  # raw frames to buffer

  # Message service (optional)
  "MESSAGE_BUFFER_SIZE": 5,
  "TIMEOUT_FRAME": 1000,
  "TIMEOUT_FLOW_CONTROL": 1000,
  "SEPARATION_TIME": 241,
  "BLOCK_SIZE": 10,
}
