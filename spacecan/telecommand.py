from .frame import Frame
from . import logger, config, ID_TC


TC_BUFFER_SIZE = config.get('TC_BUFFER_SIZE', 10)


class TelecommandProducer:

    def __init__(self, network):
        self._network = network

    def send(self, target_node_id, data):
        frame = Frame(ID_TC + target_node_id, bytearray(data))
        if self._network.is_active():
            try:
                self._network.send(frame)
            except Exception:
                pass  # fail silently


class TelecommandConsumer:

    def __init__(self, network):
        self._network = network
        network.register_telecommand_handler(self._received)
        self._buffer = []

    def _received(self, frame):
        if len(self._buffer) < TC_BUFFER_SIZE:
            self._buffer.append(frame)

    def any(self):
        return len(self._buffer) > 0

    def read(self):
        if not self.any():
            return None
        frame = self._buffer.pop()
        node_id = frame.can_id - ID_TC
        if node_id != self._network.node_id:
            logger.error(
                "received telecommand that was destined for {}".format(
                    node_id))
        return frame.data
