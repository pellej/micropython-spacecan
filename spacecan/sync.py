import pyb
from .frame import Frame
from . import config, ID_SYNC

LED_SYNC = config.get('LED_SYNC')
TIMER_SYNC = config.get('TIMER_SYNC', 4)


class Sync:
    """
    The Sync service allows sending sync frames, either manually or
    periodically. The Sync service is typically used by the network master
    node to allow slave nodes synchronize their behaviour upon receiving
    this event.

    """
    def __init__(self, network):
        self._network = network
        self._led = LED_SYNC
        self._running = False


class SyncProducer(Sync):

    def __init__(self, network):
        super().__init__(network)
        self._frame = Frame(can_id=ID_SYNC, data=bytearray())
        self._timer = pyb.Timer(TIMER_SYNC)
        self._sync_sent = False

    def send(self):
        if self._network.is_active():
            if self._led:
                pyb.LED(self._led).toggle()
            try:
                self._network.send(self._frame)
                self._sync_sent = True
            except Exception:
                pass  # fail silently

    def start(self, period):
        if self._running:
            self.stop()
        self._sync_sent = False
        self._timer.init(freq=1000/period)
        self._timer.callback(self._callback)
        self._running = True

    def stop(self):
        self._timer.deinit()
        self._timer.callback(None)
        self._running = False

    def _callback(self, timer):
        self.send()

    def sent(self):
        if not self._sync_sent:
            return False
        else:
            self._sync_sent = False
            return True


class SyncConsumer(Sync):

    def __init__(self, network):
        super().__init__(network)
        network.register_sync_handler(self._received)
        self._sync_received = False

    def start(self):
        self._sync_received = False
        self._running = True

    def stop(self):
        self._running = False

    def _received(self, frame):
        if self._running:
            if self._led:
                pyb.LED(self._led).toggle()
            self._sync_received = True

    def is_received(self):
        if not self._sync_received:
            return False
        else:
            self._sync_received = False
            return True
