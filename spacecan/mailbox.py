"""
The Mailbox service is used for sending and receiving of Message objects.

The transfer of messages typically involves the exchange of several frames
between transmitting and receiving node to manage the transfer (except for
messages with 7 or less bytes, which are sent in a single frame). For each
outbound message transfer a MessageTransferOutbound object is created, for
inbound message transfers a MessageTransferInbound is created. Transfers
get deleted once the transfer completes or is aborted.

"""
import time
import pyb
from .frame import Frame
from .message import Message, MessageBuffer
from . import config, ID_MESSAGE, NODE_MASK
from . import (
    OK,
    ERR_TIMEOUT_FRAME,
    ERR_TIMEOUT_FLOW_CONTROL,
    ERR_WRONG_SN,
    ERR_UNEXCPECTED,
    ERR_BUFFER_OVERFLOW
)

# frame types
SINGLE_FRAME = 0
FIRST_FRAME = 1
CONSECUTIVE_FRAME = 2
FLOW_CONTROL_FRAME = 3

# flow control replies
CONTINUE_TO_SEND = 0
WAIT = 1
OVERFLOW = 2

# protocol constants
SEQ_NUMBER_SIZE = 15

# configuration
SEPARATION_TIME = config.get('SEPARATION_TIME', 1)
BLOCK_SIZE = config.get('BLOCK_SIZE', 10)
MAX_WAIT_FRAMES = config.get('MAX_WAIT_FRAMES', 2)
MESSAGE_BUFFER_SIZE = config.get('MESSAGE_BUFFER_SIZE', 10)
TIMEOUT_FRAME = config.get('TIMEOUT_FRAME', 1000)
TIMEOUT_FLOW_CONTROL = config.get('TIMEOUT_FLOW_CONTROL', 1000)


def delay(value):
    if value <= 127:
        pyb.delay(value)
    elif 0xF1 <= value <= 0xF9:
        pyb.udelay((value - 0xF0) * 100)
    else:
        raise Exception("invalid separation time")


class MessageTransferOutbound:
    """
    The MessageTransferOutbound object is used to manage outgoing message
    transfers. This object is created and used by the Mailbox service. It
    buffers the data from a message and transfers the data in smaller chunks,
    frame by frame until the buffer is empty.

    """
    def __init__(self, message):
        self._buffer = message.data
        self.flow_status = None
        self.can_id = ID_MESSAGE + message.node_id
        self.total_length = len(message.data)
        self.separation_time = None
        self.block_size = None
        self.seq_number = 0

    def get_chunk(self, size):
        if len(self._buffer) < size:
            size = len(self._buffer)
        chunk = self._buffer[:size]
        self._buffer = self._buffer[size:]
        return chunk

    def has_finished(self):
        return len(self._buffer) == 0


class MessageTransferInbound:
    """
    The MessageTransferInbound object is used to manage ingoing message
    transfers. This object is created and used by the Mailbox` service. It
    buffers the data received frame by frame until the full message has
    been received.

    """
    def __init__(self, node_id):
        self._node_id = node_id
        self._buffer = bytearray()
        self.can_id = ID_MESSAGE + node_id
        self.total_length = None
        self.separation_time = SEPARATION_TIME
        self.block_size = BLOCK_SIZE
        self.block_count = 0
        self.seq_number = 0

    def add_chunk(self, data):
        self._buffer.extend(data)

    def has_finished(self):
        return len(self._buffer) == self.total_length

    def get_message(self):
        return Message(self._node_id, self._buffer)


class Mailbox:

    def __init__(self, network):
        self._network = network
        self._message_buffer = MessageBuffer(MESSAGE_BUFFER_SIZE)
        self.outbound_transfers = {}
        self.inbound_transfers = {}
        self._network.register_message_handler(self._received)
        self._running = False

    def any(self):
        return self._message_buffer.any()

    def read(self):
        return self._message_buffer.read()

    def clear(self):
        self._message_buffer.clear()

    def start(self):
        self._running = True

    def stop(self):
        self._running = False

    def is_running(self):
        return self._running

    def send(self, message):
        if len(message) < 8:
            status = self._send_single_frame_message(message)
        else:
            # create transfer object and register as ongoing transmit
            outbound_transfer = MessageTransferOutbound(message)
            self.outbound_transfers[message.node_id] = outbound_transfer
            status = self._send_multiple_frame_message(outbound_transfer)
            del self.outbound_transfers[message.node_id]
        return status

    def _send_single_frame_message(self, message):
        data = bytearray([(SINGLE_FRAME << 4) + len(message)])
        data.extend(message.data)
        can_id = ID_MESSAGE + message.node_id
        frame = Frame(can_id, data)
        try:
            self._network.send(frame, timeout=TIMEOUT_FRAME)
        except Exception:
            return ERR_TIMEOUT_FRAME
        return OK

    def _send_multiple_frame_message(self, outbound_transfer):
        # send first frame
        status = self._send_first_frame(outbound_transfer)
        if status != OK:
            return status
        # send remaining frames
        while True:
            status = self._wait_for_flow_control(outbound_transfer)
            if status != OK:
                return status
            status = self._send_block_of_consecutive_frames(outbound_transfer)
            if status != OK:
                return status
            if outbound_transfer.has_finished():
                break
        return OK

    def _send_first_frame(self, outbound_transfer):
        data = bytearray(
            [(FIRST_FRAME << 4) + (outbound_transfer.total_length >> 8)])
        data.extend(bytearray([outbound_transfer.total_length & 0xFF]))
        data.extend(outbound_transfer.get_chunk(6))  # add first 6 bytes
        frame = Frame(outbound_transfer.can_id, data)
        try:
            self._network.send(frame, timeout=TIMEOUT_FRAME)
        except Exception:
            return ERR_TIMEOUT_FRAME
        return OK

    def _wait_for_flow_control(self, outbound_transfer):
        start = time.ticks_ms()
        while True:
            if outbound_transfer.flow_status == CONTINUE_TO_SEND:
                return OK
            if outbound_transfer.flow_status == WAIT:
                start = time.ticks_ms()  # reset timeout
            elif outbound_transfer.flow_status == OVERFLOW:
                return ERR_BUFFER_OVERFLOW
            # check if timeout
            if time.ticks_diff(
                    time.ticks_ms(), start) > TIMEOUT_FLOW_CONTROL:
                return ERR_TIMEOUT_FLOW_CONTROL

    def _send_block_of_consecutive_frames(self, outbound_transfer):
        if outbound_transfer.block_size == 0:
            while True:
                status = self._send_consecutive_frame(outbound_transfer)
                if status != OK:
                    return status
                if outbound_transfer.has_finished():
                    break
                # wait as per separation time here
                delay(outbound_transfer.separation_time)
        else:
            for _ in range(outbound_transfer.block_size):
                status = self._send_consecutive_frame(outbound_transfer)
                if status != OK:
                    return status
                if outbound_transfer.has_finished():
                    break
                # wait as per separation time here
                delay(outbound_transfer.separation_time)
        return OK

    def _send_consecutive_frame(self, outbound_transfer):
        # increase sequence counter
        outbound_transfer.seq_number = (
            outbound_transfer.seq_number + 1) % SEQ_NUMBER_SIZE
        # construct consecutive frame, shuffle data into frame
        data = bytearray(
            [(CONSECUTIVE_FRAME << 4) + outbound_transfer.seq_number])
        data.extend(outbound_transfer.get_chunk(7))  # add 7 bytes (or less)
        frame = Frame(outbound_transfer.can_id, data)
        try:
            self._network.send(frame, timeout=TIMEOUT_FRAME)
        except Exception:
            return ERR_TIMEOUT_FRAME
        return OK

    def _received(self, frame):
        frame_type = frame.data[0] >> 4
        node_id = frame.can_id & NODE_MASK

        # this part handles frames related to inbound message transfer

        if frame_type == SINGLE_FRAME:
            if not self._running:
                return
            # abort any ongoing receive from same node
            if node_id in self.inbound_transfers:
                del self.inbound_transfers[node_id]
            data = frame.data[1:]
            message = Message(node_id, data)
            self._message_buffer.write(message)

        elif frame_type == FIRST_FRAME:
            inbound_transfer = MessageTransferInbound(node_id)
            # add to ongoing, also aborts an ongoing receive from same node
            if self._running:
                self.inbound_transfers[node_id] = inbound_transfer
            inbound_transfer.total_length = (
                (frame.data[0] & 0x0F) << 8) + frame.data[1]
            inbound_transfer.add_chunk(frame.data[2:])
            self._send_flow_control(inbound_transfer)

        elif frame_type == CONSECUTIVE_FRAME:
            # check if receive is ongoing
            if node_id not in self.inbound_transfers:
                return ERR_UNEXCPECTED
            inbound_transfer = self.inbound_transfers[node_id]
            seq_number = frame.data[0] & 0x0F
            inbound_transfer.seq_number = (
                inbound_transfer.seq_number + 1) % SEQ_NUMBER_SIZE
            if inbound_transfer.seq_number != seq_number:
                # abort the ongoing receive
                del self.inbound_transfers[node_id]
                return ERR_WRONG_SN
            inbound_transfer.add_chunk(frame.data[1:])
            if inbound_transfer.has_finished():
                message = inbound_transfer.get_message()
                self._message_buffer.write(message)
                del self.inbound_transfers[node_id]
                return OK
            # check if we need to send a flow control
            inbound_transfer.block_count += 1
            if inbound_transfer.block_count == inbound_transfer.block_size:
                inbound_transfer.block_count = 0
                self._send_flow_control(inbound_transfer)

        # this part handles frames related to outbound message transfer

        elif frame_type == FLOW_CONTROL_FRAME:
            flow_status = frame.data[0] & 0x0F
            # check if this is for an ongoing transfer
            if node_id in self.outbound_transfers:
                transfer = self.outbound_transfers[node_id]
                transfer.flow_status = flow_status
                if flow_status == CONTINUE_TO_SEND:
                    transfer.block_size = frame.data[1]
                    transfer.separation_time = frame.data[2]

    def _send_flow_control(self, inbound_transfer):
        flow_status = CONTINUE_TO_SEND if self._running else OVERFLOW
        data = bytearray(
            [(FLOW_CONTROL_FRAME << 4) + flow_status])
        data.extend(bytearray([inbound_transfer.block_size]))
        data.extend(bytearray([inbound_transfer.separation_time]))
        try:
            self._network.send(Frame(inbound_transfer.can_id, data))
        except Exception:
            return ERR_TIMEOUT_FLOW_CONTROL
        return OK
