from . import config


DEBUG = config.get('DEBUG', False)


class Frame:

    def __init__(self, can_id, data=None):
        self.can_id = can_id

        if data is None:
            self.data = bytearray()
        else:
            if len(data) > 8:
                raise Exception("not more than 8 data bytes allowed")
            self.data = bytearray(data)

    def __len__(self):
        return len(self.data)


class FrameBuffer:

    def __init__(self, size):
        """
        A FrameBuffer is used to buffer frames received from the Network
        over the active Bus. The FrameBuffer is designed as a FIFO and is
        useful to overcome the limited storing capability of the pyboard CAN
        controller hardware FIFO.

        """
        self._size = size
        self._data = [[0, 0, 0, memoryview(bytearray(8))] for _ in range(size)]
        self._index_write = 0
        self._index_read = 0

    def buffer_frame(self, bus):
        next_index = (self._index_write + 1) % self._size
        if next_index == self._index_read:
            # in case of overflow, discard the received frame(s)
            bus.flush_fifo()
            if DEBUG:
                raise Exception("frame buffer overflow")
        else:
            bus.can.recv(bus.fifo, list=self._data[self._index_write])
            self._index_write = next_index

    def read(self):
        if self._index_read == self._index_write:
            return None  # buffer is empty
        can_id, _, _, data = self._data[self._index_read]
        self._index_read = (self._index_read + 1) % self._size
        return Frame(can_id, data)

    def any(self):
        if self._index_read == self._index_write:
            return False
        return True

    def clear(self):
        self._index_write = 0
        self._index_read = 0
