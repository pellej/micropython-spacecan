"""
The Heartbeat service is needed for the redundancy management, to let the
master node define the active bus (either nominal or redundant one) for
communication and to let the slave nodes know which is the active bus to listen
to. For this, master node implements the HeartbeatProducer class while slave
nodes implement the HeartbeatConsumer class.

"""
import pyb
from .frame import Frame
from . import config, ID_HEARTBEAT


LED_HEARTBEAT = config.get('LED_HEARTBEAT')
TIMER_HEARTBEAT = config.get('TIMER_HEARTBEAT', 1)
HEARTBEAT_PERIOD = config.get('HEARTBEAT_PERIOD', 500)
MAX_HEARTBEAT_MISSED = config.get('MAX_HEARTBEAT_MISSED', 3)
MAX_BUS_SWITCHES = config.get('MAX_BUS_SWITCHES', False)


class Heartbeat:

    def __init__(self, network):
        self._network = network
        self._timer = pyb.Timer(TIMER_HEARTBEAT)
        self._running = False
        self._led = LED_HEARTBEAT

    def start(self, period=HEARTBEAT_PERIOD):
        if self._running:
            self.stop()
        self._timer.init(freq=1000/period)
        self._timer.callback(self._timer_callback)
        self._running = True

    def stop(self):
        self._timer.deinit()
        self._timer.callback(None)
        self._running = False


class HeartbeatProducer(Heartbeat):

    def __init__(self, network):
        super().__init__(network)
        self._frame = Frame(
            can_id=ID_HEARTBEAT + self._network.node_id,
            data=bytearray([0]))

    def _timer_callback(self, timer):
        self._send()

    def _send(self):
        if self._network.is_active() and self._led:
            pyb.LED(self._led).toggle()
        try:
            self._network.send(self._frame)
        except Exception:
            pass  # fail silently


class HeartbeatConsumer(Heartbeat):

    def __init__(
            self, network,
            max_heartbeat_missed=MAX_HEARTBEAT_MISSED,
            max_bus_switches=MAX_BUS_SWITCHES):
        super().__init__(network)
        self._max_heartbeat_missed = max_heartbeat_missed
        self._max_bus_switches = max_bus_switches
        self._heartbeat_missed = 0
        self._bus_switches = 0
        self._network.register_heartbeat_handler(self._received)

    def _timer_callback(self, timer):
        if self._network.is_active():
            self._heartbeat_missed += 1
            if self._heartbeat_missed >= self._max_heartbeat_missed:
                # check if a bus switch is allowed
                if (self._bus_switches < self._max_bus_switches) or\
                        not self._max_bus_switches:
                    self._network.switch_bus()
                    self._bus_switches += 1
                    self._heartbeat_missed = 0

    def _received(self, frame):
        if self._running:
            if self._led:
                pyb.LED(self._led).toggle()
            self._timer.counter(0)
            self._heartbeat_missed = 0
            self._bus_switches = 0
