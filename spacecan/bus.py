"""
The Bus class provides an abstraction of the microcontroller's integrated
CAN controller. There are two CAN controllers on a pyboard, hence two Bus
objects can be created, namely Bus(1) and Bus(2).

The Bus class is not directly used but is needed for creation of
the Network object class, through which receiving and sending of frames
is handled.

Internally, a Bus has a callback function that is triggered whenever
a CAN frame is received (that matches the filters) by the CAN controller.
If the current Bus object is not the active Bus of the Network,
it gets discarded. Otherwise:

- each frame is added to the FrameBuffer of the Network, which is designed
  as a software buffer to extend the CAN controller hardware buffer,
  which is very limited in size.
- a process (Network.process) is scheduled for each rame that executes
  right after the callback and calls the handler defined for the type of
  the frame that was received, provided that such handler has been
  registered.

"""
import pyb
import micropython
from .network import Network
from . import config


TOTAL_FILTERBANKS = 14
POS_FIFO_0 = 6  # position of fifo 0 information in can.info() list
DEBUG = config.get('DEBUG', False)


class Bus:
    """
    Construct a Bus object for the given channel with the given bitrate.

    - channel: One of the two CAN controllers of the pyboard (1 or 2)
    - bitrate: Bitrate in kbps

    """
    def __init__(self, channel, bitrate=1000):
        if channel not in [1, 2]:
            raise Exception("channel must be 1 or 2")
        self._channel = channel
        self._null_sink = [0, 0, 0, memoryview(bytearray(8))]
        self.fifo = channel - 1
        self.can = pyb.CAN(
            self._channel, mode=pyb.CAN.NORMAL,
            prescaler=(2000 // bitrate), sjw=1, bs1=14, bs2=6)
        self.can.initfilterbanks(TOTAL_FILTERBANKS)
        self.can.rxcallback(self.fifo, self._rxcallback)
        self.network = None

    def __repr__(self):
        return "Bus(%s)" % self._channel

    def send(self, frame, timeout=0):
        """
        Send a frame on the bus.

        - frame (Frame`): The frame to be sent.
        - timeout: If timeout is 0 the frame is placed in one of three hardware
            buffers of the pyboard and the methods returns immediately.
            If all three buffers are in use an exception is thrown. If timeout
            is not 0, the method waits until the frame is transmitted. If
            the frame can't be transmitted within the specified time an
            exception is thrown.

        """
        self.can.send(data=frame.data, id=frame.can_id, timeout=timeout)

    def _clear_filters(self):
        self.can.initfilterbanks(TOTAL_FILTERBANKS)

    def set_filters(self, filters):
        """
        Set the filters to define which frames to be received from the bus.
        Only frames with a CAN ID that match one of the filter will be
        received.

        - filters (list of tuples): Filters are provided as a list of tuples
        containing a can_id and a mask:

        >>> [(can_id, mask), (can_id, mask), ...]

        A received frame matches the filter when
        (received_can_id & mask) == (can_id & mask).

        """
        if not isinstance(filters, list):
            raise Exception("filters must be a list of tuples")

        if len(filters) > TOTAL_FILTERBANKS:
            raise Exception("too many filters provided")

        bank = 0  # start from bank 0
        params = []
        for can_filter in filters:
            if not isinstance(can_filter, tuple):
                raise Exception("filters must be a list of tuples")
            params.append(can_filter[0])
            params.append(can_filter[1])
            if len(params) == 4:
                self.can.setfilter(bank, pyb.CAN.MASK16, self.fifo, params)
                params = []
                bank += 1
        if len(params) > 0:  # complete a halfly filled params list
            params.append(0x000)
            params.append(0x7FF)
            self.can.setfilter(bank, pyb.CAN.MASK16, self.fifo, params)

    def restart(self):
        self.can.restart()

    def shutdown(self):
        self.can.deinit()

    def flush_fifo(self):
        while self.can.any(self.fifo):
            self.can.recv(self.fifo, list=self._null_sink)

    def _rxcallback(self, can, reason):
        """Callback for received frames.

        The method is called when a CAN frame is received on the bus
        that matches the filters defined for the bus. If the bus on which the
        frame was received is not the active bus (of the network) then the
        frame is discarded. Otherwise, a processing of the received frame
        is scheduled to be executed once the callback returns.
        The method raises an exception when the hardware fifo overflows.

        """
        if self.network and self.network.is_active() and\
                (self.network.get_active_bus() == self):
            # go through all received frames
            while self.can.any(self.fifo):
                # read frame into local buffer
                self.network.frame_buffer.buffer_frame(self)
                # schedule process of each pending frame
                try:
                    micropython.schedule(Network.process, self.network)
                except Exception:
                    if DEBUG:
                        raise Exception("schedule stack overflow")
            if reason == 1:
                # canbus fifo full, no further frames can be buffered
                pass
            elif reason == 2:
                # should never happen
                raise Exception("canbus fifo overflow")
        else:
            # frame received on inactive bus
            self.flush_fifo()
