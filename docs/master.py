import pyb
import time
import logging; logging.get_logger('spacecan').set_level(logging.DEBUG)
import spacecan


MY_NODE_ID = 0  # master node has id 0
SLAVE_NODE_IDS = [1, 2]
NUMBER_OF_SLAVE_NODES = len(SLAVE_NODE_IDS)


bus_a, bus_b = spacecan.Bus(1), spacecan.Bus(2)
network = spacecan.Network(MY_NODE_ID, bus_a, bus_b)
network.set_filters([(spacecan.ID_TM, spacecan.FUNCTION_MASK)])
heartbeat = spacecan.HeartbeatProducer(network)
sync = spacecan.SyncProducer(network)
telecommand = spacecan.TelecommandProducer(network)
telemetry = spacecan.TelemetryConsumer(network)

network.start()
heartbeat.start(500)
sync.start(1000)


def main():
    print("Master node started")
    pyb.LED(4).on()  # indicate the master node

    # switch bus via USER button
    def switch_callback():
        network.switch_bus()
        print("Bus switched")
        pyb.delay(200)  # short delay for switch debounce
    switch = pyb.Switch()
    switch.callback(switch_callback)
    print("Push button to switch bus")

    tick = time.ticks_ms()
    mode = 1
    while True:
        # check if telemetry from slaves arrived
        while telemetry.any():
            node, data = telemetry.read()
            print("Telemetry from {}: {}".format(node, data[:]))

        # send telecommand every few seconds to slaves
        if time.ticks_diff(time.ticks_ms(), tick) > 4100:
            tick = time.ticks_ms()
            print("Send telecommand, mode:", mode)
            for node_id in SLAVE_NODE_IDS:
                telecommand.send(node_id, [mode])
            mode = 0 if mode == 1 else 1


main()  # run the main loop
