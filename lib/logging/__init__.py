import sys


DEBUG = 1
INFO = 2
WARNING = 3
ERROR = 4

_level_str = {
    DEBUG: "DEBUG",
    INFO: "INFO",
    WARNING: "WARNING",
    ERROR: "ERROR",
}

_stream = sys.stderr


class Logger:

    def __init__(self, name, level=None):
        self.name = name
        self.level = level

    def set_level(self, level):
        self.level = level

    def debug(self, msg, *args):
        self.log(DEBUG, msg, *args)

    def info(self, msg, *args):
        self.log(INFO, msg, *args)

    def warning(self, msg, *args):
        self.log(WARNING, msg, *args)

    def error(self, msg, *args):
        self.log(ERROR, msg, *args)

    def log(self, level, msg, *args):
        if self.level is not None:
            if level >= self.level:
                msg = msg if not args else msg % args
                record = "%s:%s:%s" % (_level_str[level], self.name, msg)
                if _stream is not None:
                    _stream.write(record)
                    _stream.write("\n")


class Manager:

    def __init__(self):
        self.logger_dict = {}

    def get_logger(self, name):
        if name in self.logger_dict:
            logger = self.logger_dict[name]
        else:
            logger = Logger(name)
            self.logger_dict[name] = logger
        return logger


root = Logger('root', WARNING)
manager = Manager()


def get_logger(name=None):
    if name:
        return manager.get_logger(name)
    else:
        return root


def set_level(level):
    root.set_level(level)


def debug(msg, *args):
    root.log(DEBUG, msg, *args)


def info(msg, *args):
    root.log(INFO, msg, *args)


def warning(msg, *args):
    root.log(WARNING, msg, *args)


def error(msg, *args):
    root.log(ERROR, msg, *args)


def log(level, msg, *args):
    root.log(level, msg, *args)
